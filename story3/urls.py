from django.urls import path
from .views import home, about, register

app_name = 'story3'
urlpatterns = [
    path('', home, name='home'),
    path('about', about, name='about'),
    path('register', register, name='register'),
    
]